#
# Test of Comments
#
#
graph [
id 1
graphics [
]
directed 1
node [
id 4
graphics [
x 147.500
y 191.000
w 16.0000
h 16.0000
type "rectangle"
fill "#FFFFFF"
outline "#000000"
]
LabelGraphics [
]
edgeAnchor [
defaultFunction "None"
]
]
node [
id 7
graphics [
x 235.000
y 103.500
w 16.0000
h 16.0000
type "rectangle"
fill "#FFFFFF"
outline "#000000"
]
LabelGraphics [
]
edgeAnchor [
defaultFunction "None"
]
]
node [
id 10
graphics [
x 147.500
y 16.0000
w 16.0000
h 16.0000
type "rectangle"
fill "#FFFFFF"
outline "#000000"
]
LabelGraphics [
]
edgeAnchor [
defaultFunction "None"
]
]
node [
id 13
graphics [
x 60.0000
y 103.500
w 16.0000
h 16.0000
type "rectangle"
fill "#FFFFFF"
outline "#000000"
]
LabelGraphics [
]
edgeAnchor [
defaultFunction "None"
]
]
edge [
source 7
target 4
id 25
graphics [
x 191.250
y 147.250
w 71.5000
h 71.5000
type "line"
fill "#000000"
arrow "last"
Line [
point [
x 227.000
y 111.500
]
point [
x 155.500
y 183.000
]
]
]
LabelGraphics [
x 191.250
y 147.250
w 71.5000
h 71.5000
]
edgeAnchor [
sourceFunction "None"
targetFunction "None"
]
]
edge [
source 10
target 4
id 34
graphics [
x 147.500
y 103.500
w 4.26039e-06
h 159.000
type "line"
fill "#000000"
arrow "last"
Line [
point [
x 147.500
y 24.0000
]
point [
x 147.500
y 183.000
]
]
]
LabelGraphics [
x 147.500
y 103.500
w 4.26039e-06
h 159.000
]
edgeAnchor [
sourceFunction "None"
targetFunction "None"
]
]
edge [
source 10
target 7
id 37
graphics [
x 191.250
y 59.7500
w 71.5000
h 71.5000
type "line"
fill "#000000"
arrow "last"
Line [
point [
x 155.500
y 24.0000
]
point [
x 227.000
y 95.5000
]
]
]
LabelGraphics [
x 191.250
y 59.7500
w 71.5000
h 71.5000
]
edgeAnchor [
sourceFunction "None"
targetFunction "None"
]
]
edge [
source 13
target 4
id 43
graphics [
x 103.750
y 147.250
w 71.5000
h 71.5000
type "line"
fill "#000000"
arrow "last"
Line [
point [
x 68.0000
y 111.500
]
point [
x 139.500
y 183.000
]
]
]
LabelGraphics [
x 103.750
y 147.250
w 71.5000
h 71.5000
]
edgeAnchor [
sourceFunction "None"
targetFunction "None"
]
]
edge [
source 13
target 7
id 46
graphics [
x 147.500
y 103.500
w 159.000
h 8.52078e-06
type "line"
fill "#000000"
arrow "last"
Line [
point [
x 68.0000
y 103.500
]
point [
x 227.000
y 103.500
]
]
]
LabelGraphics [
x 147.500
y 103.500
w 159.000
h 8.52078e-06
]
edgeAnchor [
sourceFunction "None"
targetFunction "None"
]
]
edge [
source 13
target 10
id 49
graphics [
x 103.750
y 59.7500
w 71.5000
h 71.5000
type "line"
fill "#000000"
arrow "last"
Line [
point [
x 68.0000
y 95.5000
]
point [
x 139.500
y 24.0000
]
]
]
LabelGraphics [
x 103.750
y 59.7500
w 71.5000
h 71.5000
]
edgeAnchor [
sourceFunction "None"
targetFunction "None"
]
]
]

