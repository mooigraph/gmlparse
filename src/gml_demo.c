
/*
 *  Copyright t lefering
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  These are the four essential freedoms with GNU GPL software:
 *  1: freedom to run the program, for any purpose
 *  2: freedom to study how the program works, and change it to make it do what you wish
 *  3: freedom to redistribute copies to help your Free Software friends
 *  4: freedom to distribute copies of your modified versions to your Free Software friends
 *   ,           ,
 *  /             \
 * ((__-^^-,-^^-__))
 * `-_---'  `---_-'
 *  `--|o`   'o|--'
 *      \  `  /
 *       ): :(
 *       :o_o:
 *        "-"
 */


/*
 * Demo for the GML_parser. Usage: gml_demo <gml_file>
 * Simply prints the list of Key-Value pairs to stdout
 */

#include <stdio.h>
#include <stdlib.h>

#include "gml_parser.h"

void
print_keys (struct GML_list_elem *list)
{

  while (list)
    {
      printf ("%s\n", list->key);
      list = list->next;
    }
  return;
}

int
main (int argc, char *argv[])
{

  struct GML_pair *list;
  struct GML_stat *stat;
  FILE *file;

  if (argc != 2)
    {
      printf ("Usage: gml_test <gml_file> \n");
      return (0);
    }

  stat = (struct GML_stat *) calloc (1, sizeof (struct GML_stat));

  stat->key_list = NULL;

 /* open datafile */
  file = fopen (argv[1], "r");

  if (file == NULL)
    {
      printf ("\n No such file: %s", argv[1]);
      return (0);
    }


  GML_init ();

  list = GML_parser (file, stat, 0);

  if (stat->err.err_num != GML_OK)
    {
      printf
	("An error occured while reading line %d column %d of %s:\n",
	 stat->err.line, stat->err.column, argv[1]);

      switch (stat->err.err_num)
	{
	case GML_UNEXPECTED:
	  printf ("UNEXPECTED CHARACTER");
	  break;

	case GML_SYNTAX:
	  printf ("SYNTAX ERROR");
	  break;

	case GML_PREMATURE_EOF:
	  printf ("PREMATURE EOF IN STRING");
	  break;

	case GML_TOO_MANY_DIGITS:
	  printf ("NUMBER WITH TOO MANY DIGITS");
	  break;

	case GML_OPEN_BRACKET:
	  printf ("OPEN BRACKETS LEFT AT EOF");
	  break;

	case GML_TOO_MANY_BRACKETS:
	  printf ("TOO MANY CLOSING BRACKETS");
	  break;

	default:
	  printf ("Unknown error %d",stat->err.err_num);
	  break;
	}

      printf ("\n");
    }

  GML_print_list (list, 0);

  printf ("Keys used in %s: \n", argv[1]);

  print_keys (stat->key_list);

  GML_free_list (list, stat->key_list);

  return (0);
}

/* end */
